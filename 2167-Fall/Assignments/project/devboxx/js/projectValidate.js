/*a3
project.js
Fall 2016
Caleb King
*/

 function projectValidate() {

    // We need 1 JS variable for each HTML element we want to check
    var name = document.getElementById("name").value;
    var uname = document.getElementById("uname").value;
    var pword = document.getElementById("pword").value;
    var reenter = document.getElementById("reenter").value;
    var email = document.getElementById("email").value;

    var probation = document.getElementsByName("probation");
    var macpc = document.getElementsByName("macpc");
    var gitsvn = document.getElementsByName("gitsvn");

    var beers = document.getElementById("beers").value;
    var comments = document.getElementById("comments").value;


    // This variable will be set to true when a field is blank
    var blank = false;

    // This variable will be used to create our on-screen message message
    var message = "Please fill in/fix/select:";

    // Floating focus flags
    var f1 = 0;
    var f2 = 0;
    var f3 = 0;
    var f4 = 0;
    var f5 = 0;
    var f6 = 0;
    var f7 = 0;
    var f8 = 0;
    var f9 = 0;
    var fa = 0;

    // These variables will help us validate username, password, and email
    var dot = 0;
    var space = 0;
    var atSign = 0;

    // Code starts here....

    // Name
    if (name == " ") {
        document.getElementById("name").style.backgroundColor = "yellow";
        blank = true;
        message = message + "\n  Name";
        f1 = 1;

    } else {
        if (name.trim().length < 3 || name.trim().length > 30) {
            document.getElementById("name").style.backgroundColor = "red";
            document.getElementById("name").value = "";
            blank = true;
            message = message + "\n  Name: 3-30 chars cannot be all spaces!";
            f1 = 1;
        } else {
            document.getElementById("name").style.backgroundColor = "white";
        }
    }

    // Username
    if (uname == " ") {
        document.getElementById("uname").style.backgroundColor = "yellow";
        blank = true;
        message = message + "\n  Username";
        f2 = 1;
    } else {
        space = uname.indexOf(" ");
        if (space != -1 || uname.length < 5 || uname.length > 15) {
            document.getElementById("uname").style.backgroundColor = "red";
            document.getElementById("uname").value = "";
            blank = true;
            message = message + "\n  Username: 5-15 chars, no spaces!";
            f2 = 1;
        } else {
            document.getElementById("uname").style.backgroundColor = "white";
        }
    }

    // Password
    if (pword == " ") {
        document.getElementById("pword").style.backgroundColor = "yellow";
        blank = true;
        message = message + "\n  Password";
        f3 = 1;
    } else {
        space = pword.indexOf(" ");
        if (space != -1 || pword.length < 5 || pword.length > 15) {
            document.getElementById("pword").style.backgroundColor = "red";
            document.getElementById("reenter").style.backgroundColor = "red";
            document.getElementById("pword").value = "";
            document.getElementById("reenter").value = "";
            blank = true;
            message = message + "\n  password: 5-15 chars, no spaces!";
            f3 = 1;
        } else {
            document.getElementById("pword").style.backgroundColor = "white";
        }
    }

    // Re-Enter
    if (reenter == " ") {
        document.getElementById("reenter").style.backgroundColor = "yellow";
        blank = true;
        message = message + "\n  Re-enter";
        f4 = 1;
    } else {
        if (f3 == 0) {
            document.getElementById("reenter").style.backgroundColor = "white";
        }
    }

    // Check to see if password match
    if (pword != reenter) {
        document.getElementById("pword").style.backgroundColor = "red";
        document.getElementById("reenter").style.backgroundColor = "red";
        document.getElementById("pword").value = "";
        document.getElementById("reenter").value = "";
        blank = true;
        message = message + "\n  Passwords don't match!";
        f3 = 1;
    }

    // Email
    if (email == " ") {
        document.getElementById("email").style.backgroundColor = "yellow";
        blank = true;
        message = message + "\n  Email";
        f5 = 1;
    } else {
        dot = email.indexOf(".");
        space = email.indexOf(" ");
        atSign = email.indexOf("@");
        if (dot == -1 || space != -1 || atSign == -1 || email.length < 6 || email.length > 50) {
            document.getElementById("email").style.backgroundColor = "red";
            document.getElementById("email").value = "";
            blank = true;
            message = message + "\n  Email: Valid email addr only, 6-50 chars!";
            f5 = 1;
        } else {
            document.getElementById("email").style.backgroundColor = "white";
        }
    }

    // Probation
    if (probation[0].checked) {
        alert("Academic Probation!");
        document.getElementById("probation").style.backgroundColor = "#99ffcc";
    } else {
        document.getElementById("probation").style.backgroundColor = "yellow";
        // f6 = 1;
    }

    // Hot or not
    if (!(macpc[0].checked || macpc[1].checked)) {
        blank = true;
        message = message + "\n Mac or PC?";
        f7 = 1;
    }

    // Smart or not
    if (!(gitsvn[0].checked || gitsvn[1].checked)) {
        blank = true;
        message = message + "\n Git or SVN?";
        f8 = 1;
    }

    // Beers
    if (beers == " ") {
        document.getElementById("beers").style.backgroundColor = "yellow";
        blank = true;
        message = message + "\n  Favorite Beer";
        f9 = 1;
    } else {
        document.getElementById("beers").style.backgroundColor = "white";
    }

    // Comments
    if (comments == " ") {
        document.getElementById("comments").style.backgroundColor = "yellow";
        blank = true;
        message = message + "\n  Why Should I consider you";
        fa = 1;
    } else {
        if (comments.length < 2 || comments.length > 500) {
            document.getElementById("comments").style.backgroundColor = "red";
            blank = true;
            message = message + "\n  Comments between 2-500 chars (including spaces)!";
            fa = 1;
        } else {
            document.getElementById("comments").style.backgroundColor = "white";
        }
    }

    // Check to see if any fields were left blank
    if (blank) {
        // display the validate message on screen
        alert(message);

        // Check floating focus flags
        if (fa == 1) {
            document.getElementById("comments").focus();
        }
        if (f9 == 1) {
            document.getElementById("beers").focus();
        }
        if (f8 == 1) {
            document.getElementById("git").focus();
        }
        if (f7 == 1) {
            document.getElementById("mac").focus();
        }
        if (f6 == 1) {
            document.getElementById("probation").focus();
        }
        if (f5 == 1) {
            document.getElementById("email").focus();
        }
        if (f4 == 1) {
            document.getElementById("reenter").focus();
        }
        if (f3 == 1) {
            document.getElementById("pword").focus();
        }
        if (f2 == 1) {
            document.getElementById("uname").focus();
        }
        if (f1 == 1) {
            document.getElementById("name").focus();
        }

        // Return control to HTML
        return false;
    }
    // If we get to here, wass passed validation. Yay!
    return true;
}
