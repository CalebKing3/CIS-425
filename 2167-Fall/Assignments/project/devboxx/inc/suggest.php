<?php
// suggest.php
// Fall 2016
// Caleb King
// Description - Suggestion Page to allow user to provide new content

session_name('cbking1');
session_start();

 // Check to see if user is already logged in
  if(!isset($_SESSION['cbking1']))
  {
      header('Location: login.php');
      exit;
  }

 $pageTitle = "Suggest a Media Item";
 $section = "suggest";
 include("header.php");

 ?>
 <div class="section page">
     <div class="main">
         <h1>Suggest a Media Item</h1>
         <p>If you think there is something I&rsquo;m missing, let me know! Complete the form to send me an email.</p>
         <div class="suggestForm">
           <form id="suggestform" action="confirmSuggestion6.php" method="post">
             <p>Registration Form</p>
             <p>
               <!-- Name -->
               <label for="name">Name:</label>
               <input type="text" id="name" name="name"
               autofocus
               required
               title="Name: 3-30 chars, letters and - , space, and ' only!"
               pattern="[a-zA-Z0-9-' ]{3,30}"
               placeholder="Name"
               />
               <br />


               <!-- Yay Email! -->
               <label for="email">Email Address:</label>
               <input type="email" id="email" name="email"
               required
               title="Valid E-mail address only!"
         			 pattern="[a-zA-Z0-9-_$.]+@[a-z0-9-_.]+\.[a-z]{2,16}"
               maxlength="50"
               onfocus="hint(this.id)"
               placeholder="Email"
               />
               <br />


               <!-- . Yay suggestions! -->
               <label for="category">Category:</label>
               <select id="category" name="category"
                  required
                  title="Pick which category your suggestion fits in">
                 <option value="">Select a category...</option>
                 <option value="book">Book</option>
                 <option value="tutorial">Tutorial</option>
                 <option value="podcast">Podcast</option>
               </select>
               <br />

               <!-- comments-->
               <label for="suggestions">Suggestion:</label><br />
               <textarea id="suggestions" name="suggestions" rows="3" cols="48"
               required
               title="Please enter comments (500 characters max)"
               maxlength="500"
               placeholder="suggestions"></textarea>
               <br />
             </p>

             <!-- Submit button -->
             <p class="submit">
                 <input type="submit" value="SUBMIT"/>
                 <span class="reset">
                   <input type="reset" value="CLEAR!" onclick="history.go(0)"/>
               </span>
             </p>
           </form>

        </div>
     </div>
 </div>
 <?php include("footer.php"); ?>
