<?php
    // Devboxx
    // process.php
    // Fall 2016
    // Caleb King

    // Connect to db (LOCAL OR SERVER)
    // include('local-connect.php');
    include('server-connect.php');

    // get username and password from login form
    $uname = mysqli_real_escape_string($dbc, $_POST['uname']);
    $pword = mysqli_real_escape_string($dbc, $_POST['pword']);

    // Build username query for new db
    $query = "SELECT * FROM hw7 WHERE uname = '$uname'";

    // Run username query
    $result = mysqli_query($dbc, $query) or die('login error:' . mysqli_error($dbc));

    // See if we got a row
    if(mysqli_num_rows($result) == 0)
    {
        // username invalid - return to login and message
        header('Location: login.php?rc=1');
        exit;
    }

    // If we got here, we can verify username. Yay!
    // Store results of uname query for pword verification
    $row = mysqli_fetch_array($result);

    // hash and check user-entered pword against stored and hashed pword
    if(password_verify($pword, $row['pword']) == $row['pword'])
    {
          session_id('cbking1');
          session_name('cbking1');
          session_start();

          // set php session variable
          $_SESSION['cbking1'] = $row['name'];

          // close db connection
          mysqli_close($dbc);

          // transfer control to welcome page
          header('Location: welcome.php');
          exit;
    }
    else
    {
          header('Location: login.php?rc=2');
          exit;
    }
