<!DOCTYPE html>
<html lang="en">
<!--
Devboxx
usernameNotAvail.php
Fall 2016
Caleb King
-->
 <head>

   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheets/main.css" />

   <!-- Web Title -->
  <title>Devboxx - User Not Available</title>

 </head>
 <body>
      <div class="header">
        <div class="main">
          <h1 class="branding-title"><a href="project/devboxx/inc/login.php"></a></h1> <!--using css to upload image -->
          <ul class="nav">
                    <li><a href="index.php">CIS-Home</a></li>
                </ul>
        </div>
      </div>
      <div id="content"> <!--using this div to contain the body element -->

      <div id="main">
        <p class="bold">SORRY!</p>
        <p class="bold">Username [
            <span class="red"><?php echo @$_GET['uname']; ?> </span>
          ] is not available!</p>
        <p class="bold">Please click <a href="project/devboxx/inc/checkUsername.php">HERE</a>
          to see if your desired Username is available, or click <a href="project/devboxx/inc/register.php">HERE</a>
          to return to the Registration Form and try again.
        </p>
        <p class="note">Please check to see if your desired Username is available BEFORE registering!</p>
      </div>

      <div id="prefooter"></div>
      <?php include('../inc/footer.php'); ?>
