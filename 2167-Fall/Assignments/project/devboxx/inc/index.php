<?php
/*
index.php
Fall 2016
Caleb King
Description - body content to randomize data from all categories
 */

 session_id('cbking1');
 session_name('cbking1');
 session_start();

  // Check to see if user is already logged in
   if(!isset($_SESSION['cbking1']))
   {
       header('Location: login.php');
       exit;
   }

 include("data.php");
 include("functions.php");
 $pageTitle = "Devboxx Media";
 $section = null; // Landing page is set to null since its the first page
 include("header.php");
 ?>

 		<div class="section catalog random">
 			<div class="main">
 				<h2>Welcome to Devboxx my list of favorite developer resources</h2>
         <ul class="items">
             <?php
             $random = array_rand($catalog,4);
             foreach ($random as $id) {
                 echo get_item_html($id,$catalog[$id]);
             }
             ?>
 				</ul>
 			</div>
 		</div>
 <?php include("footer.php"); ?>
