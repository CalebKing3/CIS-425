<?php
// about.php
// Fall 2016
// Caleb King
// Description - about devboxx

// Continue session from process.php
// session_id('cbking1');
session_name('cbking1');
session_start();

 // Check to see if user is already logged in
  if(!isset($_SESSION['cbking1']))
  {
      header('Location: login.php');
      exit;
  }

 $pageTitle = 'About Us';
 $section = 'about';
 include('header.php');

 ?>
 <div class="section page">
     <div class="main">
         <h1>About Us</h1>
         <p>Welcome to Devboxx! My name is Caleb King and I'm a software developer by day, gym rat at night, sports and dog fantatic.
             I was studying computer information systems at Arizona State University. However, not putting my best effor at the number one party school
             I did not graduate which forced me into a different career path.
           </p>
           <br />
              <p>
                I orignally thought I was going to be a business analyst or excel wizard but I ended up being a self taught engineer
               but it wasn't easy. At first my "strengths" were in HTML, CSS and javascript but I only had a small grasp of how technology worked.
               I didn't have the money or time to go back and get a computer science degree...so I began to research how other people learned to code.
              </p>
              <br />

             <p>
               Over the past four years I've found different sites of tutorials, podcast and books that have really help establish a career in the software engineering industry.
             </p>
             <br />

             <p>
               I've had the chance to work at National Academy of Sports Medicine(NASM), Ticketmaster.com and now a startup called WebPT.
               Now it's time for me to share that knowledge with future developers looking to break into the field as well.
             </p>
             <br />

             <p>
               With the help of these resources I've been able to increase my skill set, salary and confidence within the industry. Now I aspire to become an entrepreneur and build SaaS products.
             </p>
             <br />

             <p>
               Learning is a life long process, here at Devboxx we encourage people to send their suggestions to our site to help improve our content. Thanks for visiting!
             </p>
             <br />
     </div>
 </div>
 <?php include('footer.php'); ?>
