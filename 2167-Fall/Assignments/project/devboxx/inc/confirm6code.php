<?php
  // Devboxx
  // confirm6code.php
  // fall 2016
  // Caleb King

  // pull element values in from html
  // name
  $oname = $_POST['name'];

  $name = mysqli_real_escape_string($dbc, $oname);

  // username
  $uname = mysqli_real_escape_string($dbc, $_POST['uname']);

  // password
  $pword = mysqli_real_escape_string($dbc, $_POST['pword']);

  // salt and hash password
  $pword = password_hash($pword, PASSWORD_DEFAULT);

  // email
  $email = mysqli_real_escape_string($dbc, $_POST['email']);

  // probation
  $prob = @$_POST['prob'];

  // git or svn
  $gitsvn = @$_POST['gitsvn'];

  // mac or pc
  $macpc = @$_POST['macpc'];

  // beers
  $languages = @$_POST['languages'];

  // comments
  $comments = mysqli_real_escape_string($dbc, $_POST['comments']);

  // verify unique usernames
  $check = mysqli_query($dbc, "select id from hw7 where uname = '$uname'")
    or die('confirm6 read error' . mysqli_close($dbc));
  if (mysqli_num_rows($check) != 0){
    header("Location: usernameNotAvail.php?&uname='$uname'");
    exit;
    }

  // build a query
  $query = "insert into hw7(name,uname,pword,email,prob,macpc,gitsvn,languages,comments)" . "values('$name', '$uname', '$pword','$email','$prob','$macpc','$gitsvn','$languages','$comments')";

  // run query
  $result = mysqli_query($dbc, $query) or die('DB Write error: ' .mysqli_error($dbc));

  // close db connections
  mysqli_close($dbc);
?>
