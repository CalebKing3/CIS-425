<?php
// Devboxx
// welcome.php
// Fall 2016
// Caleb King


  // Continue session from process.php
  session_id('cbking1');
  session_name('cbking1');
  session_start();

   // Check to see if user is already logged in
    if(!isset($_SESSION['cbking1']))
    {
        header('Location: login.php');
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
    else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

    <!-- Meta tag -->
    <meta charset="utf-8" />

    <meta http-equiv="refresh" content="5; url=index.php" />

    <!-- favicon link -->
    <link type="image/gif" rel="icon" href="images/home3.png" />

    <!-- link tag for css -->
    <link type="text/css" rel="stylesheet" href="stylesheets/main.css" />

    <!-- Web Title -->
    <title>Devboxx - Welcome</title>

</head>
<body>
<div id="wrapper">
    <?php include('../inc/header.php'); ?>

    <div id="main">
        <p class="bold">Hello! You are logged in as <?php echo $_SESSION['cbking1']; ?>.</p>
        <p class="bold">Please wait 5 seconds to be redirected to our landing page.</p>
    </div>

    <div id="prefooter"></div>
    <!-- footer in case a user clicks on make a donation before redirection-->
      <div class="footer">
        <div class="main">
          <ul class="footerList">
            <li><a href="http://twitter.com/kingcaleb3" target="_blank">Twitter</a></li>
            <li><a href="https://www.gitlab.com/calebking3" target="_blank">Gitlab</a></li>
            <!-- <li><a href="https://calebking3.gitlab.com/macDevSetup" target="_blank">Mac Setup</a></li> -->
            <li><a href="project/devboxx/inc/jcart-1.3/index.php">Make a Donation</a></li>
          </ul>
          <p class="footerTitle">&copy;<?php echo date("Y"); ?> Devboxx Media</p>
        </div>
      </div>
    </body>
    </html>
