<!DOCTYPE html>
<html lang="en">
<!--
Devboxx
register.php
Fall 2016
Caleb King
-->
 <head>

   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="../../images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheets/main.css" />

   <!-- javascript tags -->
   <script src="project/devboxx/js/projectHints.js" type="text/javascript"></script>

   <!-- Web Title -->
  <title>Devboxx - Register</title>

 </head>
 <body>
      <div class="header">
        <div class="main">
          <h1 class="branding-title">.<a href="project/devboxx/inc/login.php"></a></h1> <!--using css to upload image -->
          <ul class="nav">
              <li><a href="index.php">CIS-Home</a></li>
              <li><a href="project/devboxx/inc/checkUsername.php">Username</a></li>
              <?php
                 if(isset($_SESSION['cbking1'])  &&  $_SESSION['cbking1'] != "")
                       echo '<li><a href="project/devboxx/inc/logout.php">logout</a></li>';
                 else
                       echo '<li><a href="project/devboxx/inc/login.php">login</a></li>';
              ?>
            </ul>
        </div>
      </div>
      <div id="content"> <!--using this div to contain the body element -->

      <div id="main">
        <p class="bold">Thank you for your interest in joining Devboxx!</p>
        <p class="note">Is your username avalibale?!?</p>
      </div>

      <form id="registerForm" action="project/devboxx/inc/confirm6.php" method="post">
        <p>Registration Form</p>
        <p>
          <!-- Name -->
          <label for="name">Name:</label>
          <input type="text" id="name" name="name"
          autofocus
          required
          title="Name: 3-30 chars, letters and - , space, and ' only!"
          pattern="[a-zA-Z0-9-' ]{3,30}"
          onfocus="hint(this.id)"
          placeholder="Name"
          />
          <br />

          <!-- Username -->
          <label for="uname">Username:</label>
          <input type="text" id="uname" name="uname"
          required
          title="Username: 5-15 chars, letters, 0-9 -, _, !, or $ only! no spaces"
          pattern="[a-zA-Z0-9-_!$]{5,15}"
          onfocus="hint(this.id)"
          placeholder="Username"
          />
          <br />

          <!-- Password -->
          <label for="pword">Password:</label>
          <input type="password" id="pword" name="pword"
          required
          title="Password: 4-15 chars, upper/lower case and -, _, !, $ ' only!"
          pattern="[a-zA-Z0-9-_!$]{4,15}"
          onchange="form.reenter.pattern=this.value;"
          onfocus="hint(this.id)"
          placeholder="Password"
          />
          <br />

          <!-- Re-enter -->
          <label for="reenter">Re-enter Password:</label>
          <input type="password" id="reenter" name="reenter"
          required
          title="Password must match!"
          onfocus="hint(this.id)"
          placeholder="Re-enter"
          />
          <br />

          <!-- Email -->
          <label for="email">Email Address:</label>
          <input type="email" id="email" name="email"
          required
          title="Valid E-mail address only!"
          pattern="[a-zA-Z0-9-_$.]+@[a-z0-9-_.]+\.[a-z]{2,16}"
          maxlength="50"
          onfocus="hint(this.id)"
          placeholder="Email"
          />
          <br />

          <!-- Probation -->
          <label for="probation">Academic Probation?</label>
          <input type="checkbox" id="probation" name="probation"
          title="I don't care if you are on probation"
          onfocus="hint(this.id)" onclick="hint(this.id)"/>
          <br />

          <!-- Mac or PC -->
          <label for="mac">Mac or PC?</label>
          Mac <input type="radio" id="mac" name="macpc" value="mac" required />
          PC <input type="radio" id="pc" name="macpc" value="pc"
          title="Mac Rules!!!"
          onfocus="hint(this.id)" onclick="hint(this.id)" />
          <br />

          <!-- Git or SVN -->
          <label for="git">Git or SVN?</label>
          Git <input type="radio" id="git" name="gitsvn" value="git" required />
          SVN <input type="radio" id="svn" name="gitsvn" value="svn"
          title="Yay! Git is awesome!!!"
          onfocus="hint(this.id)" onclick="hint(this.id)" />
          <br />

          <!-- Language -->
          <label for="languages">Language:</label>
          <select id="languages" name="languages" required title="You like to code!">
            <option value="">Select a programming language...</option>
            <option value="java">Java</option>
            <option value="javascript">Javascript</option>
            <option value="ruby">Ruby</option>
            <option value="csharp">C#</option>
          </select>
          <br />

          <!-- comments-->
          <label for="comments">Comments:</label><br />
          <textarea id="comments" name="comments" rows="3" cols="48"
          required
          title="Please enter comments (500 characters max)"
          maxlength="500"
          placeholder="Comments"></textarea>
          <br />
        </p>

        <!-- Submit button -->
        <p class="submit">
            <input type="submit" value="SUBMIT"/>
            <span class="reset">
              <input type="reset" value="CLEAR!" onclick="history.go(0)"/>
          </span>
          <span class="check">
            <input type="submit" value="CHECK USERNAME" onclick="window.location.href='checkUsername.php'"/>
        </span>
        </p>
      </form>

      <p id="jsmsgs"></p>
      <div id="prefooter"></div>
      <?php include('../inc/footer.php'); ?>
