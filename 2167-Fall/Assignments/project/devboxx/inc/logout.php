<?php
// Devboxx
// logout.php
// Fall 2016
// Caleb King

  session_id('cbking1');
  session_name('cbking1');
  session_start();
  session_unset();
  session_destroy('cbking1');
  $_SESSION = array();

  //need to fix this when i figure out logout working successfully
  header('Location: login.php');
  exit;
