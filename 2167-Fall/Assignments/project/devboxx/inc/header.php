<?php
session_name('cbking1');
session_start();
?>
<!DOCTYPE html>
<!--
header.php
Fall 2016
Caleb King
Description - head page to contain nav bar
-->
<html lang="en">
<head>
	<title><?php echo $pageTitle; ?></title> <!-- using php variable to display page title -->
	<!-- <link rel="stylesheet" href="../css/main.css" type="text/css"> -->
	<link type="text/css" rel="stylesheet" href="../../../stylesheets/main.css" />
</head>
<body>
	<div class="header">
		<div class="main">
			<h1 class="branding-title">.<a href="index.php"></a></h1>
			<ul class="nav">
                <li class="books<?php if ($section == "books") { echo " on"; } ?>"><a href="catalog.php?cat=books">Books</a></li>
                <li class="tutorials<?php if ($section == "tutorial") { echo " on"; } ?>"><a href="catalog.php?cat=tutorial">Tutorial</a></li>
                <li class="podcast<?php if ($section == "podcast") { echo " on"; } ?>"><a href="catalog.php?cat=podcast">Podcast</a></li>
                <li class="suggest<?php if ($section == "suggest") { echo " on"; } ?>"><a href="suggest.php">Suggest</a></li>
                <li class="about<?php if ($section == "about") { echo " on"; } ?>"><a href="about.php">About</a></li>
                <?php
         			 		if(isset($_SESSION['cbking1'])  &&  $_SESSION['cbking1'] != "")
         	     			    echo '<li><a href="logout.php">logout</a></li>';
         					else
         								echo '<li><a href="login.php">login</a></li>';
         			 ?>
            </ul>
		</div>
	</div>
	<div id="content"> <!--using this div to contain the body element -->
