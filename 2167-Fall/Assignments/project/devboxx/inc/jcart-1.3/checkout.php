<?php
session_name('cbking1');

// jCart v1.3
// http://conceptlogic.com/jcart/
// This file demonstrates a basic checkout page
// If your page calls session_start() be sure to include jcart.php first
include_once('jcart/jcart.php');
session_start();

if(!isset($_SESSION['cbking1']))
{
    header('Location: ../login.php');
    exit;
}
?>


<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Devboxx - Donation</title>
		<link rel="stylesheet" type="text/css" media="screen, projection" href="style.css" />
		<link rel="stylesheet" type="text/css" media="screen, projection" href="jcart/css/jcart.css" />
		<link rel="stylesheet" type="text/css" media="screen, projection" href="../../../../stylesheets/main.css" />
	</head>
	<body>
    	<div class="header">
    		<div class="main">
    			<h1 class="branding-title">.<a href="../index.php"></a></h1>
    			<ul class="nav">
                    <li class="books<?php if ($section == "books") { echo " on"; } ?>"><a href="../catalog.php?cat=books">Books</a></li>
                    <li class="tutorials<?php if ($section == "tutorial") { echo " on"; } ?>"><a href="../catalog.php?cat=tutorial">Tutorial</a></li>
                    <li class="podcast<?php if ($section == "podcast") { echo " on"; } ?>"><a href="../catalog.php?cat=podcast">Podcast</a></li>
                    <li class="suggest<?php if ($section == "suggest") { echo " on"; } ?>"><a href="../suggest.php">Suggest</a></li>
                    <li class="about<?php if ($section == "about") { echo " on"; } ?>"><a href="../about.php">About</a></li>
                    <?php
             			 		if(isset($_SESSION['cbking1'])  &&  $_SESSION['cbking1'] != "")
             	     			    echo '<li><a href="../logout.php">logout</a></li>';
             					else
             								echo '<li><a href="login.php">login</a></li>';
             			 ?>
          </ul>
    		</div>
    	</div>
    	<div id="content"> <!--using this div to contain the body element -->
    		<div id="wrapper">
    			<h2>Checkout</h2>
    			<div id="sidebar"></div>
    			<div id="content">
    				<div id="jcart"><?php $jcart->display_cart();?></div>
    				<p class="pCart"><a href="index.php">&larr; Continue shopping</a></p>
    				<?php
    					//echo '<pre>';
    					//var_dump($_SESSION['jcart']);
    					//echo '</pre>';
    				?>
    			</div>
    			<div class="clear"></div>
    		</div>
		<script type="text/javascript" src="jcart/js/jquery-1.4.4.min.js"></script>
		<script type="text/javascript" src="jcart/js/jcart.min.js"></script>
    <?php include('../footer.php'); ?>
