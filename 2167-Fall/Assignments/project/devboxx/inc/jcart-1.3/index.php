<?php
session_name('cbking1');

// Check to see if user is already logged in


// jCart v1.3
// http://conceptlogic.com/jcart/

// This file demonstrates a basic store setup

// If your page calls session_start() be sure to include jcart.php first
include_once 'jcart/jcart.php';

session_start();

if(!isset($_SESSION['cbking1']))
{
    header('Location: ../login.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Devboxx - Donation</title>
		<link rel="stylesheet" type="text/css" media="screen, projection" href="style.css" />
		<link rel="stylesheet" type="text/css" media="screen, projection" href="jcart/css/jcart.css" />
		<link rel="stylesheet" type="text/css" media="screen, projection" href="../../../../stylesheets/main.css" />
	</head>
	<body>
    	<div class="header">
    		<div class="main">
    			<h1 class="branding-title">.<a href="../index.php"></a></h1>
    			<ul class="nav">
                    <li class="books<?php if ($section == "books") { echo " on"; } ?>"><a href="../catalog.php?cat=books">Books</a></li>
                    <li class="tutorials<?php if ($section == "tutorial") { echo " on"; } ?>"><a href="../catalog.php?cat=tutorial">Tutorial</a></li>
                    <li class="podcast<?php if ($section == "podcast") { echo " on"; } ?>"><a href="../catalog.php?cat=podcast">Podcast</a></li>
                    <li class="suggest<?php if ($section == "suggest") { echo " on"; } ?>"><a href="../suggest.php">Suggest</a></li>
                    <li class="about<?php if ($section == "about") { echo " on"; } ?>"><a href="../about.php">About</a></li>
                    <?php
             			 		if(isset($_SESSION['cbking1'])  &&  $_SESSION['cbking1'] != "")
             	     			    echo '<li><a href="../logout.php">logout</a></li>';
             					else
             								echo '<li><a href="login.php">login</a></li>';
             			 ?>
                </ul>
    		</div>
    	</div>
    	<div id="content"> <!--using this div to contain the body element -->

		<div id="wrapper">
			<h2 id="donationTitle">Donation Store</h2>
      <p class="thankYouDonation">Thank you for considering a donation to Devboxx. These donations are used 100% to improve the site's user experience.</p>

			<div id="sidebar">
				<div id="jcart"><?php $jcart->display_cart(); ?></div>
			</div>

			<div id="content">

				<form method="post" action="" class="jcart">
					<fieldset>
						<input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
						<input type="hidden" name="my-item-id" value="ABC-123" />
						<input type="hidden" name="my-item-name" value="Donation1" />
						<input type="hidden" name="my-item-price" value="25.00" />
						<input type="hidden" name="my-item-url" value="" />

						<ul class="donationList">
							<li>Donation Level 1</li>
							<li id="price1">Price: $25.00</li>
							<li>
								<label class="quantityCart">Qty: <input type="text" name="my-item-qty" value="1" size="3" /></label>
							</li>
						</ul>

						<input class="jcartInput" type="submit" name="my-add-button" value="add to cart" class="button" />
					</fieldset>
				</form>

				<form method="post" action="" class="jcart">
					<fieldset>
						<input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
						<input type="hidden" name="my-item-id" value="2" />
						<input type="hidden" name="my-item-name" value="Donation2" />
						<input type="hidden" name="my-item-price" value="50.00" />
						<input type="hidden" name="my-item-url" value="" />

						<ul>
							<li>Donation Level 2</li>
							<li id="price2">Price: $50.00</li>
							<li>
								<label class="quantityCart">Qty: <input type="text" name="my-item-qty" value="1" size="3" /></label>
							</li>
						</ul>

						<input class="jcartInput" type="submit" name="my-add-button" value="add to cart" class="button" />
					</fieldset>
				</form>

				<form method="post" action="" class="jcart">
					<fieldset>
						<input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
						<input type="hidden" name="my-item-id" value="3" />
						<input type="hidden" name="my-item-name" value="Donation3" />
						<input type="hidden" name="my-item-price" value="75.00" />
						<input type="hidden" name="my-item-url" value="" />

						<ul>
							<li>Donation Level 3</li>
							<li id="price3">Price: $75.00</li>
							<li>
								<label class="quantityCart">Qty: <input type="text" name="my-item-qty" value="1" size="3" /></label>
							</li>
						</ul>

						<input class="jcartInput" type="submit" name="my-add-button" value="add to cart" class="button tip" />
					</fieldset>
				</form>

				<div class="clear"></div>

				<p><small>Having trouble? <a href="jcart/server-test.php">Test your server settings.</a></small></p>

				<?php
          //echo '<pre>';
          //var_dump($_SESSION['jcart']);
          //echo '</pre>';
        ?>
			</div>

		<div class="clear"></div>
		</div>
		<script type="text/javascript" src="jcart/js/jquery-1.4.4.min.js"></script>
		<script type="text/javascript" src="jcart/js/jcart.min.js"></script>
    <?php include '../footer.php'; ?>
