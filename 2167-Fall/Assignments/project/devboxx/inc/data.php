<?php
/*
Devboxx
data.php
Fall 2016
Caleb King
Description - Abstracted data from index.php


NOTE - the number convention is trying to follow hundreds place to represent catagory.
and the ones place to represent item number in that catalog.
Learn this from teamtreehouse inc course. link in refence README.md
For example catalog[101] is first book in book catalog
            catalog[201] is first Tutorial in tutorial catalog, etc.
 */

// TODO clarify if there are added benefits beside readability between array() or []
// TODO research/find php function to convert arrays => json objects.
$catalog = [];

//=====================================================
/////////////////////Books////////////////////////////
//=====================================================

$catalog[101] = [
  "title" => "Soft Skills: The software developer life manual",
	"category" => "books",
	"genre" => "Business/Career Development",
	"format" => "Paperback",
	"year" => 2014,
    "isbn" => '978-1617292392',
    "authors" => ["John Z. Sonmez"], // making this an array so i add multiple
    "img" => "../img/media/softskills.jpg", // php convention is leave trailing comma
];


$catalog[102] = [
    "title" => "Clean Code: A Handbook of Agile Software Craftsmanship",
    "category" => "books",
    "genre" => "Technology",
    "format" => "Paperback",
    "year" => 2008,
    "isbn" => '978-0132350884',
    "authors" => ["Robert C. Martin"],
    "img" => "../img/media/clean_code.jpg",
];


$catalog[103] = [
    "title" => "Eloquent JavaScript: A Modern Introduction to Programming",
    "category" => "books",
    "genre" => "Technology",
    "format" => "Paperback",
    "year" => 2011,
    "isbn" => '978-1593272821',
    "authors" => ["Marijn Haverbeke"],
    "img" => "../img/media/eloquentJS.jpg",
];


$catalog[104] = [
    "title" => "Head First Java",
    "category" => "books",
    "genre" => "Technology",
    "format" => "Books",
    "year" => 2003,
    "isbn" => '007-6092046981',
    "authors" => ["Bert Bates","Kathy Sierra"],
    "img" => "../img/media/headFirstJava.jpg",
];



//=====================================================
/////////////////////Tutorial Resources////////////////
//=====================================================

$catalog[201] = [
    "title" => "Teamtreehouse",
    "category" => "tutorial",
    "genre" => "Technology",
    "format" => "Video",
    "year" => 2011,
    "founder" => ["Ryan Carson"],
    "img" => "../img/media/teamtreehouse.png",
];


$catalog[202] = [
    "title" => "Tutorialspoint",
    "category" => "tutorial",
    "genre" => "Technology",
    "format" => "Writing Instructions",
    "year" => 2011,
    "founder" => ["Mohtashim M."],
    "img" => "../img/media/tutorialspoint.png",
];


$catalog[203] = [
    "title" => "Codecademy",
    "category" => "tutorial",
    "genre" => "Technology",
    "format" => "Code-along",
    "year" => 2013,
    "founder" => ["Zach Sims","Ryan Bubinski"],
    "img" => "../img/media/codecademy.png",
];


$catalog[204] = [
    "title" => "CodeSchool",
    "category" => "tutorial",
    "genre" => "Technology",
    "format" => "Video/Code-along",
    "year" => 2012,
    "founder" => ["Gregg Pollack"],
    "img" => "../img/media/codeschool.png",
];


//=====================================================
/////////////////////Podcast///////////////////////////
//=====================================================

$catalog[301] = [
    "title" => "Javascript Jabber",
    "category" => "podcast",
    "genre" => "Technology",
    "format" => "DevChatTV",
    "year" => 2012,
    "platform" => "DevChatTV",
    "img" => "../img/media/javascript_jabber.jpg",
];


$catalog[302] = [
    "title" => "Software Engineering Radio",
    "category" => "podcast",
    "genre" => "Technology",
    "format" => "Talkshow",
    "year" => 2006,
    "platform" => "iTunes",
    "img" => "../img/media/sofwareEngineeringRadio.jpg",
];


$catalog[303] = [
    "title" => "Coding Blocks",
    "category" => "podcast",
    "genre" => "Technology",
    "format" => "Interview",
    "year" => 2013,
    "platform" => "iTunes",
    "img" => "../img/media/codingblocks.jpg",
];


$catalog[304] = [
    "title" => "The Startup Chat",
    "category" => "podcast",
    "genre" => "Startup/Tech",
    "format" => "Lecture",
    "year" => 2015,
    "platform" => "iTunes",
    "img" => "../img/media/startupChatPodcast.jpg",
];
