<!DOCTYPE html>
<!--
Devboxx
login.php
Fall 2016
Caleb King
-->

<html lang="en">
 <head>
   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="../images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheets/main.css" />

   <!-- javascript tags -->
   <script src="project/devboxx/js/projectHints.js" type="text/javascript"></script>

   <!-- Web Title -->
  <title>Devboxx - Login</title>

 </head>
 <body>
      <div class="header">
    		<div class="main">
    			<h1 class="branding-title">.<a href="project/index.php"></a></h1> <!--using css to upload image -->
    			<ul class="nav">
							<li><a href="project/index.php">CIS-Home</a></li>
							<li><a href="project/devboxx/inc/register.php">Register</a></li>
              <?php
                 if(isset($_SESSION['cbking1'])  &&  $_SESSION['cbking1'] != "")
                    echo '<li><a href="project/devboxx/inc/logout.php">logout</a></li>';
                 else
                    echo '<li><a href="project/devboxx/inc/login.php">login</a></li>';
              ?>
          </ul>
    		</div>
    	</div>
    	<div id="content"> <!--using this div to contain the body element -->

      <div id="main">
        <p class="bold">Please enter your Username and Password below…</p>
        <p class="note">Please check to see if your desired Username is available BEFORE registering!</p>
      </div>

        <form id="loginForm" action="project/devboxx/inc/process.php" method="post">
        <p>Login Form</p>
        <p class="logger">
          <?php
            if(isset($_GET['rc']))
            {
              // Check return code from process.inc
              if($_GET['rc'] == 1)
                echo 'Wrong Username!';
              if($_GET['rc'] == 2)
                echo 'Wrong Password';
              if($_GET['rc'] == 3)
                echo 'Return from process...';
            }
          ?>
        </p>

        <p>
          <!-- Username -->
          <label for="uname">Username:</label>
          <input type="text" id="uname" name="uname"
          autofocus
          required
          title="Username: 5-15 chars, letters, 0-9 -, _, !, or $ only! no spaces"
          pattern="[a-zA-Z0-9-_!$]{5,15}"
          onfocus="hint(this.id)"
          placeholder="Username"
          />
          <br />

          <!-- Password -->
          <label for="pword">Password:</label>
          <input type="password" id="pword" name="pword"
          required
          title="Password: 4-15 chars, upper/lower case and -, _, !, $ ' only!"
          pattern="[a-zA-Z0-9-_!$]{4,15}"
          onfocus="hint(this.id)"
          placeholder="Password"
          />
          <br />

        <!-- Submit button -->
        <p class="submit">
            <input type="submit" value="Submit"/>
            <span class="reset">
              <input type="reset" value="Clear!" onclick="history.go(0)"/>
          </span>
          <span class="check">
            <input type="submit" value="Register" onclick="window.location.href='register.php'"/>
        </span>
        </p>
      </form>
    <p id="jsmsgs"></p>
    <div id="prefooter"></div>
  </div><!-- end content -->

  <!-- footer -->
  	<div class="footer">
  		<div class="main">
  			<ul class="footerList">
  				<li><a href="http://twitter.com/kingcaleb3" target="_blank">Twitter</a></li>
  				<li><a href="https://www.gitlab.com/calebking3" target="_blank">Gitlab</a></li>
          <!-- <li><a href="https://calebking3.gitlab.com/macDevSetup" target="_blank">Mac Setup</a></li> -->
          <li><a href="project/devboxx/inc/jcart-1.3/index.php">Make a Donation</a></li>
  			</ul>
  			<p class="footerTitle">&copy;<?php echo date("Y"); ?> Devboxx Media</p>
  		</div>
  	</div>
  </body>
  </html>
