<?php
    // Devboxx
    // uncheck.php
    // Fall 2016
    // Caleb King

    // connect to db (LOCAL or Server)
    // include('local-connect.php');
    include('server-connect.php');

    // get username
    $uname = mysqli_real_escape_string($dbc, $_POST['uname']);

    // build & execute query from new db
    $check = mysqli_query($dbc, "select id from hw7 where uname = '$uname'");

    // Check to see if we got rows(didnt get a hit so its available)
    if(mysqli_num_rows($check) == 0)
      header("Location: checkUsername.php?&uname=$uname&status=AVAILABLE");
    else
      header("Location: checkUsername.php?&uname=$uname&status=UNAVAILABLE");
