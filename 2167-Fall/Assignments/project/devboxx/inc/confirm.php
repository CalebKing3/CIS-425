<!DOCTYPE html>
<!--
Devboxx
confirm.php
Fall 2016
Caleb King
-->
<html lang="en">
 <head>
   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />
   <meta name="robots" content="noindex, nofollow" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheets/main.css" />

   <!-- Web Title -->
  <title>Devboxx - Confirm</title>

 </head>
 <body>
    <div id="wrapper">
      <?php include('../inc/header.php'); ?>

      <div id="main">
        <p class="bold">Thank you for registering!</p>
        <p class="bold">You may use the links above to continue browsing our site.</p>
      </div>

      <div id="prefooter"></div>
      <?php include('../inc/footer.php'); ?>
    </div>
 </body>
</html>
