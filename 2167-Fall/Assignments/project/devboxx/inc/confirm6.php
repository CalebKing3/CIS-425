<?php
/*
 Devboxx
 confirm6.php
 Fall 2016
 Caleb King
*/
    // connect to db
    // change this for local or class server!!
    // include('local-connect.php');
    include('server-connect.php');

    // Set timezone
    date_default_timezone_set('MST');

    //get and store the Time Of Day
    $time = date("G");

    // include the rest of the confrim6 inc code
    include('confirm6code.php');

 ?>
 <!DOCTYPE html>
  <html lang="en">
  <head>
    <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>
      <!-- Meta tag -->
      <meta charset="utf-8" />
      <meta http-equiv="refresh" content="5; url=login.php" />
      <meta name="robots" content="noindex, nofollow" />

      <!-- favicon link -->
      <link type="image/gif" rel="icon" href="images/home3.png" />

      <!-- link tag for css -->
      <link type="text/css" rel="stylesheet" href="stylesheets/main.css" />

      <!-- Web Title -->
      <title>Devboxx - Confirm</title>
  </head>

  <body>
    <div id="wrapper">
    <?php include('../inc/header.php'); ?>

        <div id="main">
            <p class="bold">Thank you for registering with Devboxx!</p>
            <p class="bold">In 5 seconds you will be redirected back to the login page.</p>
        </div>

        <div id="prefooter"></div>
        <?php include('../inc/footer.php'); ?>
    </div>
  </body>
  </html>
