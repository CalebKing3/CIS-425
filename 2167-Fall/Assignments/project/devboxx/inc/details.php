<?php
/*
details.php
Fall 2016
Caleb King
Description - Displays additional content depending on book,tutorial or podcast
 */

 include("data.php");
 include("functions.php");

// Code to get item from data.php.
// Initally had this all in one file but thought abstraction would be ideal if i wanted to add more
// http://php.net/manual/en/reserved.variables.get.php
 if (isset($_GET["id"])) {
     $id = $_GET["id"];
     if (isset($catalog[$id])) {
         $item = $catalog[$id];
     }
 }

// Nullcheck
 if (!isset($item)) {
     header("location:catalog.php");
     exit;
 }

// This allows me to keep my title dynamic using the catalog html....thank you treehouse/codecademy
 $pageTitle = $item["title"];
 $section = null;
 include("header.php");
 ?>

 <div class="section page">
     <div class="main">

       <!-- &gt; is the > sign -->
         <div class="breadcrumbs">
             <a href="catalog.php">Full Catalog</a>
             &gt; <a href="catalog.php?cat=<?php echo strtolower($item["category"]); ?>">
                    <?php echo $item["category"]; ?>
                  </a>
             &gt;   <?php echo $item["title"]; ?>
         </div>

         <div class="media-picture">
         <span>
             <img src="<?php echo $item["img"]; ?>" alt="<?php echo $item["title"]; ?>" />
         </span>
       </div>

         <div class="media-details">
             <h1><?php echo $item["title"]; ?></h1>
             <table>
                 <tr>
                     <th>Category</th>
                     <td><?php echo $item["category"]; ?></td>
                 </tr>
                 <tr>
                     <th>Genre</th>
                     <td><?php echo $item["genre"]; ?></td>
                 </tr>
                 <tr>
                     <th>Format</th>
                     <td><?php echo $item["format"]; ?></td>
                 </tr>
                 <tr>
                     <th>Year</th>
                     <td><?php echo $item["year"]; ?></td>
                 </tr>
                 <?php if (strtolower($item["category"]) == "books") { ?>
                 <tr>
                     <th>Authors</th>
                     <td><?php echo implode(", ",$item["authors"]); ?></td>
                 </tr>
                 <tr>
                     <th>ISBN</th>
                     <td><?php echo $item["isbn"]; ?></td>
                 </tr>
                 <?php } else if (strtolower($item["category"]) == "tutorial") { ?>
                 <tr>
                     <th>Founder</th>
                     <td><?php echo $item["founder"]; ?></td>
                 </tr>
                 <?php } else if (strtolower($item["category"]) == "podcast") { ?>
                 <tr>
                     <th>Platform</th>
                     <td><?php echo $item["platform"]; ?></td>
                 </tr>
                 <?php } ?>
             </table>
         </div>
     </div>
 </div>
 <?php include("footer.php"); ?>
