[Devboxx]
====================

This is my project for my CIS-425 class with Dennis Anderson at ASU.
Devboxx is a dynamic site using html, css and php to display content for developers to use and improve their skill set.

Below are my references of sites and resources I used while making this site work.
I've included all links associated with making this project to avoid the possibility of breaking `student intergrity`

## Resources

1. Teamtreehouse
 - https://teamtreehouse.com/library/build-a-basic-php-website,
 - https://teamtreehouse.com/library/php-basics-2,
 - https://teamtreehouse.com/library/php-arrays-and-control-structures
 - https://teamtreehouse.com/library/objectoriented-php-basics-2
 - https://teamtreehouse.com/library/mac-local-php-dev-environment
 - https://teamtreehouse.com/library/php-standards-and-best-practices
  - I use Treehouse to stay relevant with programming today.
  - This is where I learn how to use the random display content at refresh, understand more php functions, discovered php.net
  - After taking some of these courses it gave me the idea to make Devboxx
  - When grading similar code will be found in the following
    - devboxx landing page(index.php) with the use of catagories to filter content
    - main.css(specifically the section layout with the idea to float images left and content right)
    - function.php (how to randomize content on refresh, hot to get items into html(custom function))
    - details.php (how to use php echo function to display any html element you want for info)
    - data.php (organizing data into an structure array and then display content depending on array key attributes)
  - The difference is my file structure is completely different
  - Organization of catalog(mine is better :-P)
  - Having multiple db's talking to my login, register and suggest forms.
    -No code was copied and paste directly, either followed their tutorials, reviewed php.net or w3school.com documentations of each function used in project(except for the custom get_item_html, which came from teamtreehouse php basic course)
    and removed anything I felt was over engineering.


2. Codecademy
 - https://www.codecademy.com/learn/php


3. ## Actually resource for content
   - I use a lot of these resources to help with my own skill set. Tried my best to levage what I've learn
   - clean code - https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882
   - Code school - https://www.codeschool.com/
   - Codecademy - https://www.codecademy.com/learn
   - CodingBlocks.net - https://www.codingblocks.net/
   - Eloquent JS - http://eloquentjavascript.net/ (it's free)
   - Head first java - https://www.amazon.com/Head-First-Java-Kathy-Sierra/dp/0596009208
   - Javascript Jabber - https://devchat.tv/js-jabber
   - Soft Skills: The software developer's life manual https://www.amazon.com/Soft-Skills-software-developers-manual/dp/1617292397
   - Software Engineering radio - http://www.se-radio.net/
   - Start up chat - http://thestartupchat.com/
   - Tutorial points - https://www.tutorialspoint.com/
