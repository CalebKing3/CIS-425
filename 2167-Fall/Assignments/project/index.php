<!DOCTYPE html>
<html lang="en">
<!--
Project links
index.php
Fall 2016
Caleb King
Description - Landing page for Devboxx
-->
 <head>

   <?php
      if (substr($_SERVER['HTTP_HOST'], 0, 9) == 'localhost') {
          echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      } else {
          echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
      }
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="images/home3.png" />

    <!--link tag for css  TODO move styles to main.css in project folder-->
   <link type="text/css" rel="stylesheet" href="stylesheets/a6ss.css" />

   <!-- javascript tags -->
   <script src="js/hints.js" type="text/javascript"></script>

   <!-- Web Title -->
  <title>Caleb King - Project Links</title>

 </head>
 <body>
    <div id="wrapper">
      <?php include '../php/header.php'; ?>

      <div id="main">
        <p class="bold">Caleb King - Project Langing Page</p>
      </div>

      <!-- Table element for links -->
      <table class="projectTable" cellspacing="0">
          <tr>
            <td id="projectTableTitle" colspan="4">Project Links</td>
          </tr>
          <tr>
            <!-- To force the anchor tags to open a new window we add a target="_new" to take control away from the user -->
            <td><a href="project/part1/ERD.php" alt="ERD">ERD</a></td>
            <td><a href="project/part1/GanttChart.php" alt="Gannt Chart">Project Plan</a></td>
            <td><a href="project/part1/DevboxxWriteUp.pdf" download="DevboxxWriteUp.pdf">Write Up</a></td>
            <td><a href="project/devboxx/inc/login.php" alt="Devboxx">Devboxx Login</a></td>
          </tr>
          <br  />
      </table>
      <br />
      <table class="competitorsTable" cellspacing="0">
        <tr>
          <td id="competitorTableTitle" colspan="3">Competitors</td>
        </tr>
        <tr>
          <!-- To force the anchor tags to open a new window we add a target="_new" to take control away from the user -->
          <td><a target="_new" id="competitorCodeCademy " href="https://www.codecademy.com/learn" alt="Codecademy">
            <img src="project/part1/codecademyLogo.svg" alt="CodecademyLogo">
          </a></td>
          <td><a target="_new" id="competitorEnvato "href="https://tutsplus.com/" alt="Envato">
            <img src="project/part1/tutsPlusLogo.svg" alt="TutsPlus">
          </a></td>
          <td><a target="_new" id="competitorSafari "href="http://techbus.safaribooksonline.com/" alt="Safari Online">
            <img src="project/part1/safariOnline.png" alt="Safari Online"
          </a></td>
        </tr>
      </table>



      <p id="jsmsgs"></p>
      <div id="prefooter"></div>
      <?php include '../php/footer.htm'; ?>
    </div>
 </body>
</html>
