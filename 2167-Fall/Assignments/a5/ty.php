<!DOCTYPE html>
<html lang="en">
<!--
a5
ty.php thank you page
Fall 2016
Caleb King
-->
 <head>

   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheets/a4ss.css" />

   <!-- Web Title -->
  <title>Caleb King - A5 Thank You</title>

 </head>
 <body>
    <div id="wrapper">
      <?php include ('../php/header.php'); ?>
      <div id="main">
        <p class="bold">Thank you for contacting us -  we will respond as soon as we can...</p>
      </div>
      <div id="prefooter"></div>
      <?php include('../php/footer.htm'); ?>
    </div>
 </body>
</html>
