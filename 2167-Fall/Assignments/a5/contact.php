<!DOCTYPE html>
<html lang="en">
<!--
a5
contact.php
Fall 2016
Caleb King
-->
 <head>

   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheets/a4ss.css" />

   <!-- javascript tags -->
   <script src="js/hints.js" type="text/javascript"></script>

   <!-- Web Title -->
  <title>Caleb King - A5</title>

 </head>
 <body>
    <div id="wrapper">
      <?php include ('../php/header.php'); ?>
      <div id="main">
        <p class="bold">Please complete the form below and click the "SEND EMAIL"</p>
      </div>

      <form id="regform" action="https://webapp4.asu.edu/pubtools/Mail" method="post">
        <p>Registration Form</p>
        <p>
          <!-- 3 hidden elements for smtp email processing -->

          <!-- this element controls where the email goes -->
          <!-- <input type="hidden" name="sendto" value="caleb.king@asu.edu" /> -->
          <input type="hidden" name="sendto" value="dennis.anderson@asu.edu" />

          <!-- this element controls the subject line of the email -->
          <input type="hidden" name="subject" value="a5 contact submission" />

          <!-- this element controls where the smtp processor replies -->
          <input type="hidden" name="location" value="http://cis425.wpcarey.asu.edu/cbking1/a5/ty.php" />


          <!-- Name -->
          <label for="name">Name:</label>
          <input type="text" id="name" name="name"
          autofocus
          required
          title="Name: 3-30 chars, letters and - , space, and ' only!"
          pattern="[a-zA-Z-' ]{3,30}"
          onfocus="hint(this.id)"/>
          <br />

          <!-- Email -->
          <label for="email">Email Address:</label>
          <input type="email" id="email" name="email"
          required
          title="Valid E-mail address only!"
    			pattern="[a-zA-Z0-9-_$.]+@[a-z0-9-_.]+\.[a-z]{2,16}"
          maxlength="50"
          onfocus="hint(this.id)" />

          <!-- comments-->
          <label for="comments">Comments:</label><br />
          <textarea id="comments" name="comments" rows="3" cols="48"
          required
          title="Please enter comments (500 characters max)"
          maxlength="500"></textarea>
          <br />
        </p>

        <!-- Submit button -->
        <p class="submit">
            <input type="submit" value="SEND EMAIL"/>
            <span class="reset">
              <input type="reset" value="CLEAR!" onclick="history.go(0)"/>
          </span>
        </p>
      </form>

      <div id="prefooter"></div>
      <?php include('../php/footer.htm'); ?>
    </div>
 </body>
</html>
