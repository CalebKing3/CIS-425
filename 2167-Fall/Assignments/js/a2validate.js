/* This is what a multi-line
comment looks like
It look just like a comment in CSS.
That's because it is the same
*/

// This is a single line comment
// a2validation.js
// Caleb King

function a2validate() {

    // We need 1 JS variable for each HTML element we want to check
    // User contact information
    var name = document.getElementById("name").value;
    var username = document.getElementById("uname").value;
    var password = document.getElementById("pword").value;
    var email = document.getElementById("email").value;

    // Radio Boxes
    var probation = document.getElementsByName("probation");
    var hotornot = document.getElementsByName("hon");
    var smartornot = document.getElementsByName("son");

    // Dropdown and textarea
    var beers = document.getElementById("beers").value;
    var comments = document.getElementById("comments").value;

    // This variable will be set to true when a field is blank
    var blank = false;

    // This variable will be used to create our on-screen message message
    var message = "Please fill in/fix/select:";

    // Code starts here....

    // Name
    if (name == "") {
        blank = true;
        message = message + "\n  Name";
    }
    // Username
    if (username == "") {
        blank = true;
        message = message + "\n  Username";
    }
    // Password
    if (password == "") {
        blank = true;
        message = message + "\n  Password";
    }
    // Email
    if (email == "") {
        blank = true;
        message = message + "\n  Email";
    }
    // Probation
    if (probation[0].checked) {
        alert("Probation!");
    }

    // Hot or not
    if (!(hotornot[0].checked || hotornot[1].checked)) {
        blank = true;
        message = message + "\n  Hot or not?";
    }

    // Smart or not
    if (!(smartornot[0].checked || smartornot[1].checked)) {
        blank = true;
        message = message + "\n  Smart or not?";
    }

    // Beers
    if (beers == "") {
        blank = true;
        message = message + "\n  Favorite Beer";
    }

    // comments
    if (comments == "") {
        blank = true;
        message = message + "\n  Comments";
    }
    // Check to see if any fields were left blank
    if (blank) {
        alert(message);
        return false;
    }
    // If we get to here, wass passed validation. Yay!
    return true;
}
