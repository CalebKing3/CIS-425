// This is a single line comment
// a3Focus.js
// Caleb King

function a3Focus() {

	// Give focus to the name field
	document.getElementById("name").focus();

	// Clear the colors
	document.getElementById("name").style.backgroundColor="white";
	document.getElementById("uname").style.backgroundColor="white";
	document.getElementById("pword").style.backgroundColor="white";
	document.getElementById("reenter").style.backgroundColor="white";
	document.getElementById("email").style.backgroundColor="white";
	document.getElementById("probation").style.backgroundColor="#99ffcc";
	document.getElementById("hony").style.backgroundColor="#99ffcc";
	document.getElementById("honn").style.backgroundColor="#99ffcc";
	document.getElementById("sony").style.backgroundColor="#99ffcc";
	document.getElementById("sonn").style.backgroundColor="#99ffcc";
	document.getElementById("beers").style.backgroundColor="white";
	document.getElementById("comments").style.backgroundColor="white";

}
