/*
a1
meter.js
Fall 2016
Caleb King
*/

/*
This is a multi-line comment in JS.
It looks just like a comment in CSS.
*/
// Javascript also has single-line comments - they start with '//'

function meter(x) {
	if (x >=1) document.getElementById("prog1").value = x;
}
