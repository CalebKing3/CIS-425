<!DOCTYPE html>
<html lang="en">
<!--
a4
index.php
Fall 2016
Caleb King
-->
 <head>

   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheets/a4ss.css" />

   <!-- javascript tags -->
   <script src="js/hints.js" type="text/javascript"></script>

   <!-- Web Title -->
  <title>Caleb King - A4</title>

 </head>
 <body>
    <div id="wrapper">
      <?php include ('../php/header.php'); ?>

      <div id="main">
        <p class="bold">Thank you for your interest in joining my CIS-425 study group</p>
        <p class="note">Please complete the registration form below and click 'SUBMIT' button to get started</p>
      </div>

      <form id="regform" action="php/confirm.php" method="get">
        <p>Registration Form</p>
        <p>
          <!-- Name -->
          <label for="name">Name:</label>
          <input type="text" id="name" name="name"
          autofocus
          required
          title="Name: 3-30 chars, letters and - , space, and ' only!"
          pattern="[a-zA-Z-' ]{3,30}"
          onfocus="hint(this.id)"/>
          <br />

          <!-- Username -->
          <label for="uname">Username:</label>
          <input type="text" id="uname" name="uname"
          required
          title="Username: 5-15 chars, letters, 0-9 -, _, !, or $ only! no spaces"
          pattern="[a-zA-Z0-9-_!$]{5,15}"
          onfocus="hint(this.id)" />
          <br />

          <!-- Password -->
          <label for="pword">Password:</label>
          <input type="password" id="pword" name="pword"
          required
          title="Password: 5-15 chars, upper/lower case and -, _, !, $ ' only!"
          pattern="[a-zA-Z0-9-_!$]{5,15}"
          onchange="form.reenter.pattern=this.value;"
          onfocus="hint(this.id)" />
          <br />

          <!-- Re-enter -->
          <label for="reenter">Re-enter Password:</label>
          <input type="password" id="reenter" name="reenter"
          required
          title="Password must match!"
          onfocus="hint(this.id)" />
          <br />

          <!-- Email -->
          <label for="email">Email Address:</label>
          <input type="email" id="email" name="email"
          required
          title="Valid E-mail address only!"
    			pattern="[a-zA-Z0-9-_$.]+@[a-z0-9-_.]+\.[a-z]{2,16}"
          maxlength="50"
          onfocus="hint(this.id)" />
          <br />

          <!-- Probation -->
          <label for="probation">Academic Probation?</label>
          <input type="checkbox" id="probation" name="probation"
          title="I don't care if you are on probation"
          onfocus="hint(this.id)" onclick="hint(this.id)"/>
          <br />

          <!-- Hot or not -->
          <label for="hony">Hot or not?</label>
          Yes <input type="radio" id="hony" name="hon" value="yes"/>
          No <input type="radio" id="honn" name="hon" value="no"
          title="Yay!!!"
          onfocus="hint(this.id)" onclick="hint(this.id)" />
          <br />

          <!-- Smart or not -->
          <label for="sony">Smart or not?</label>
          Yes <input type="radio" id="sony" name="son" value="yes"/>
          No <input type="radio" id="sonn" name="son" value="no"
          title="Yay! you are smart!!!"
          onfocus="hint(this.id)" onclick="hint(this.id)" />
          <br />

          <!-- Beers. Yay beer! -->
          <label for="beers">Favorite Beer:</label>
          <select id="beers" name="beers" required title="You might be an alcoholic">
            <option value="">Select a beer...</option>
            <option value="heineken">Heineken</option>
            <option value="pbr">Coors Light</option>
            <option value="newcastle">Pabst Blue Ribbon</option>
            <option value="coors">Red's Apple Ale</option>
          </select>
          <br />

          <!-- comments-->
          <label for="comments">Comments:</label><br />
          <textarea id="comments" name="comments" rows="3" cols="48"
          required
          title="Please enter comments (500 characters max)"
          maxlength="500"></textarea>
          <br />
        </p>

        <!-- Submit button -->
        <p class="submit">
            <input type="submit" value="SUBMIT"/>
            <span class="reset">
              <input type="reset" value="CLEAR!" onclick="history.go(0)"/>
          </span>
        </p>
      </form>

      <p id="jsmsgs"></p>
      <div id="prefooter"></div>
      <?php include('../php/footer.htm'); ?>
    </div>
 </body>
</html>
