<!DOCTYPE html>
<html lang="en">
<!--
a6
usernameNotAvail.php
Fall 2016
Caleb King
-->
 <head>

   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheets/a6ss.css" />

   <!-- Web Title -->
  <title>Caleb King - A6-userNotAvail</title>

 </head>
 <body>
    <div id="wrapper">
      <?php include ('../php/header.php'); ?>

      <div id="main">
        <p class="bold">SORRY!</p>
        <p class="bold">Username [
            <span class="red"><?php echo @$_GET['uname']; ?> </span>
          ] is not available!</p>
        <p class="bold">Please click <a href="php/checkUsername.php">HERE</a>
          to see if your desired Username is available, or click <a href="a6/">HERE</a>
          to return to the Registration Form and try again.
        </p>
        <p class="note">Please check to see if your desired Username is available BEFORE registering!</p>
      </div>

      <div id="prefooter"></div>
      <?php include('../php/footer.htm'); ?>
    </div>
 </body>
</html>
