<!--
a1
header.php
Fall 2016
Caleb King
-->
<?php
		//check to see if session is active - if not, start it
		if(!isset($_SESSION))
		{
			session_id("cbking1");
			session_name("cbking1");
			session_start("cbking1");
		}
?>

<header>
	<ul>
	     <li><a href="index.php"><img src="images/home3.png" alt="Home" /></a></li>
	     <li><a href="a2/">a2</a></li>
	     <li><a href="a3/">a3</a></li>
	     <li><a href="a4/">a4</a></li>
	     <li><a href="a5/contact.php">a5</a></li>
	     <li><a href="a6/">a6</a></li>
	     <li><a href="a7/">a7</a></li>
	     <li><a href="a8/">a8</a></li>
	     <li><a href="project/index.php">Project</a></li>
	     <li><a href="https://validator.w3.org/nu/#textarea" target="_blank">html</a></li>
	     <li><a href="http://jigsaw.w3.org/css-validator/#validate_by_input" target="_blank">CSS</a></li>
	     <li><a href="http://esprima.org/demo/validate.html" target="_blank">js</a></li>
	     <li><a href="http://phpcodechecker.com/" onclick="window.open(this.href); return false">phpx</a></li>

			 <?php
			 		if(isset($_SESSION['cbking1'])  &&  $_SESSION['cbking1'] != "")
	     			    echo '<li><a href="php/logout.php">logout</a></li>';
					else
						    echo '<li><a href="php/login.php">login</a></li>';
			 ?>
	</ul>
</header>
