<?php
  // a6
  // confirmProject6code.php
  // fall 2016
  // caleb King

  // pull element values in from html
  // name
  $oname = $_POST['name'];

  $name = mysqli_real_escape_string($dbc, $oname);

  // username
  $uname = mysqli_real_escape_string($dbc, $_POST['uname']);

  // password
  $pword = mysqli_real_escape_string($dbc, $_POST['pword']);

  // salt and hash password
  $pword = password_hash($pword, PASSWORD_DEFAULT);

  // email
  $email = mysqli_real_escape_string($dbc, $_POST['email']);

  // probation
  $prob = @$_POST['prob'];

  // hot or not
  $hon = @$_POST['hon'];

  // smart or not
  $son = @$_POST['son'];

  // beers
  $beers = @$_POST['beers'];

  // comments
  $comments = mysqli_real_escape_string($dbc, $_POST['comments']);

  // verify unique usernames
  $check = mysqli_query($dbc, "select id from hw6 where uname = '$uname'")
    or die('confirm6 read error' . mysqli_close($dbc));
  if (mysqli_num_rows($check) != 0){
    header("Location: usernameNotAvail.php?&uname='$uname'");
    exit;
    }

  // build a query
  $query = "insert into hw6(name,uname,pword,email,prob,hot,smart,beers,comments)" . "values('$name', '$uname', '$pword','$email','$prob','$hon','$son','$beers','$comments')";

  // run query
  $result = mysqli_query($dbc, $query) or die('DB Write error: ' .mysqli_error($dbc));

  // close db connections
  mysqli_close($dbc);
?>
