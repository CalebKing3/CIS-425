<!DOCTYPE html>
<html lang="en">
<!--
a6
checkusername.php
Fall 2016
Caleb King
-->
 <head>

   <?php
      if (substr($_SERVER['HTTP_HOST'], 0, 9) == 'localhost') {
          echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      } else {
          echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
      }
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheets/a6ss.css" />

   <!-- javascript tags -->
   <script src="js/hints.js" type="text/javascript"></script>

   <!-- Web Title -->
  <title>Caleb King - Check Username</title>

 </head>
 <body>
    <div id="wrapper">
      <?php include '../php/header.php'; ?>

      <div id="main">
        <p class="bold">Enter your desired Username below and we'll tell you if it's available!</p>
      </div>

      <form id="regform" action="php/uncheck.php" method="post">
        <p>Check Username Form</p>
        <p>

          <!-- Username -->
          <label for="uname">Username:</label>
          <input type="text" id="uname" name="uname"
          autofocus
          required
          title="Username: 5-15 chars, letters, 0-9 -, _, !, or $ only! no spaces"
          pattern="[a-zA-Z0-9-_!$]{5,15}"
          onfocus="hint(this.id)"
          placeholder="Username to check"
          />
          <br />

          <!-- Username check -->
          <label for="unchecked">Username Checked:</label>
          <input type="text" id="unchecked" name="unchecked"
          disabled
          placeholder="Username checked"
          value="<?php echo @$_GET['uname']; ?>"
          />
          <br />

          <!-- Status -->
          <label for="unstatus">Username Status:</label>
          <input type="text" id="unstatus" name="unstatus"
          disabled
          placeholder="Username status"
          value="<?php echo @$_GET['status']; ?>"
          />
          <br />

        <!-- Submit button -->
        <p class="submit">
            <input type="submit" value="CHECK IT!"/>
            <span class="check">
              <input type="submit" value="BACK TO REGISTER" onclick="window.location.href='../a6'"/>
            </span>
        </p>
      </form>

      <p id="jsmsgs"></p>
      <div id="prefooter"></div>
      <?php include '../php/footer.htm'; ?>
    </div>
 </body>
</html>
