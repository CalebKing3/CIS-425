<?php
// a7
// welcome.php
// Fall 2016
// Caleb King

  // Continue session from process.php
  session_id('cbking1');
  session_name('cbking1');
  session_start();

   // Check to see if user is already logged in
    if(!isset($_SESSION['cbking1']))
    {
        header('Location: login.php');
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
    else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

    <!-- Meta tag -->
    <meta charset="utf-8" />

    <!-- favicon link -->
    <link type="image/gif" rel="icon" href="images/home3.png" />

    <!-- redirect link -->
    <meta http-equiv="refresh" content="8; url=../index.php" />

    <!-- link tag for css -->
    <link type="text/css" rel="stylesheet" href="stylesheets/a7ss.css" />

    <!-- Web Title -->
    <title>Caleb King - Welcome</title>

</head>
<body>
<div id="wrapper">
    <?php include ('../php/header.php'); ?>

    <div id="main">
        <p class="bold">Hello! You are logged in as <?php echo $_SESSION['cbking1']; ?>.</p>
    </div>

    <div id="prefooter"></div>
    <?php include('../php/footer.htm'); ?>
</div>
</body>
</html>
