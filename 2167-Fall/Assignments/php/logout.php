<?php
// a7
// logout.php
// Fall 2016
// Caleb King

  session_id('cbking1');
  session_name('cbking1');
  session_start('cbking1');
  session_unset();
  session_destroy('cbking1');
  $_SESSION = array();
  header('Location: ../');
  exit;
