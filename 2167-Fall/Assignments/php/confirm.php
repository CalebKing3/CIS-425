
<!DOCTYPE html>
<html lang="en">
<!--
a2
confirm.php
Fall 2016
Caleb King
-->
 <head>

   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />
   <meta name="robots" content="noindex, nofollow" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheets/a2ss.css" />

   <!-- javascript tags -->
   <script src="js/meter.js" type="text/javascript"></script>
   <!-- Web Title -->
  <title>Caleb King - CONFIRM</title>

 </head>
 <body>
    <div id="wrapper">
      <?php include ('../php/header.php'); ?>

      <div id="main">
        <p class="bold">Thank you for registering!</p>
        <p class="bold">You may use the links above to continue browsing our site.</p>
      </div>

      
      <div id="prefooter"></div>
      <?php include('../php/footer.htm'); ?>
    </div>
 </body>
</html>
