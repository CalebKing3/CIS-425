<?php
/*
 a6
 confirmProject6.inc
 Fall 2016
 Caleb King
*/
    // connect to db
    // change this for local or class server!!
    // include('local-connect.php');
    include('server-connect.php');

    // Set timezone
    date_default_timezone_set('MST');

    //get and store the Time Of Day
    $time = date("G");

    // include the rest of the confrim6 inc code
    include('confirm6code.php');

 ?>
  <html lang="en">

  <head>
    <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>
      <!-- Meta tag -->
      <meta charset="utf-8" />
      <meta http-equiv="refresh" content="5; url=login.php" />
      <meta name="robots" content="noindex, nofollow" />
      <!-- favicon link -->
      <link type="image/gif" rel="icon" href="images/home3.png" />
      <!-- link tag for css -->
      <link type="text/css" rel="stylesheet" href="stylesheets/a6ss.css" />
      <!-- javascript tags -->
      <script src="js/meter.js" type="text/javascript"></script>
      <!-- Web Title -->
      <title>Caleb King - CONFIRM6</title>
  </head>

  <body>
    <div id="wrapper">
    <?php include ('../php/header.php'); ?>
        <div id="main">
            <p class="bold">Thank you for registering!</p>
            <p class="bold">You may use the links above to continue browsing our site.</p>
        </div>
        <div id="prefooter"></div>
        <?php include('../php/footer.htm'); ?>
    </div>
  </body>
  </html>
