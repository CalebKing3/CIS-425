<!DOCTYPE html>
<!--
final
login.inc
Fall 2016
Caleb King
-->
<?php
  // Start a PHP Session
  session_id('cbking1');
  session_name('cbking1');
  session_start('cbking1');

  // Check to see if user is already logged in
  if(isset($_SESSION['cbking1'])){
    header('Location: welcome.inc');
    exit;
  }
?>


<html lang="en">
 <head>

   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Final/cd/grading"';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheets/a7ss.css" />

   <!-- javascript tags -->
   <script src="js/hints.js" type="text/javascript"></script>

   <!-- Web Title -->
  <title>Caleb King - Login</title>

 </head>
 <body>
    <div id="wrapper">
      <?php include ('../php/header.php'); ?>

      <div id="main">
        <p class="bold">Thank you for your interest in joining my CIS-425 study group</p>
        <p class="note">Please check to see if your desired Username is available BEFORE registering!</p>
      </div>

      <form id="regform" action="php/process.php" method="post">
        <p>Login Form</p>
        <p class="logger">
          <?php
            if(isset($_GET['rc'])){
              // Check return code from process.inc
              if($_GET['rc'] == 1)
                echo 'Invalid Username!';
              if($_GET['rc'] == 2)
                echo 'Invalid Password';
              if($_GET['rc'] == 3)
                echo 'Return from process...';
            }
          ?>
        </p>
        <p>

          <!-- Username -->
          <label for="uname">Username:</label>
          <input type="text" id="uname" name="uname"
          autofocus
          required
          title="Username: 5-15 chars, letters, 0-9 -, _, !, or $ only! no spaces"
          pattern="[a-zA-Z0-9-_!$]{5,15}"
          onfocus="hint(this.id)"
          placeholder="Username"
          />
          <br />

          <!-- Password -->
          <label for="pword">Password:</label>
          <input type="password" id="pword" name="pword"
          required
          title="Password: 4-15 chars, upper/lower case and -, _, !, $ ' only!"
          pattern="[a-zA-Z0-9-_!$]{4,15}"
          onchange="form.reenter.pattern=this.value;"
          onfocus="hint(this.id)"
          placeholder="Password"
          />
          <br />

        <!-- Submit button -->
        <p class="submit">
            <input type="submit" value="SUBMIT"/>
            <span class="reset">
              <input type="reset" value="CLEAR!" onclick="history.go(0)"/>
          </span>
          <span class="check">
            <input type="submit" value="BACK TO REGISTER" onclick="window.location.href='a7/index.inc'"/>
        </span>
        </p>
      </form>

      <p id="jsmsgs"></p>
      <div id="prefooter"></div>
      <?php include('../php/footer.htm'); ?>
    </div>
 </body>
</html>
