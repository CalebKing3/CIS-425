<!DOCTYPE html>
<html lang="en">
<!--
a7
register.php
Fall 2016
Caleb King
-->
 <head>

   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Final/cd/grading"';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheet/style.css" />

   <!-- javascript tags -->
   <script src="hints.js" type="text/javascript"></script>

   <!-- Web Title -->
  <title>Caleb King - A7</title>

 </head>
 <body>
    <div id="wrapper">
      <?php include ('header.php'); ?>
      <form id="regform" action="Final/confirm6.php" method="post">
        <p>Registration Form</p>
        <p>

          <!-- Username -->
          <label for="uname">UserID</label>
          <input type="text" id="userid" name="userid"
          required
          title="Username: 5-15 chars, letters, 0-9 -, _, !, or $ only! no spaces"
          pattern="[a-zA-Z0-9-_!$]{5,15}"
          onfocus="hint(this.id)"
          placeholder="Username"
          />
          <br />

          <!-- Password -->
          <label for="pword">Password:</label>
          <input type="password" id="pword" name="pword"
          required
          title="Password: 4-15 chars, upper/lower case and -, _, !, $ ' only!"
          pattern="[a-zA-Z0-9-_!$]{4,15}"
          onchange="form.reenter.pattern=this.value;"
          onfocus="hint(this.id)"
          placeholder="Password"
          />
          <br />


          <!-- Achievement -->
          <label for="hony">Today I will achieve</label>
          World Peace <input type="radio" id="hony" name="aon" value="yes"/>
          Feed the Gators <input type="radio" id="hony" name="aon" value="yes"/>
          Wash the Unicorns <input type="radio" id="honn" name="aon" value="no"
          title="Yay!!!"
          onfocus="hint(this.id)" onclick="hint(this.id)" />
          <br />


        <!-- Submit button -->
        <p class="submit">
            <input type="submit" value="Register"/>
            <span class="reset"><input type="reset" value="Start Over!" onclick="history.go(0)"/></span>
        </p>
      </form>

      <p id="jsmsgs"></p>
      <div id="prefooter"></div>
      <?php include('footer.htm'); ?>
    </div>
 </body>
</html>
