<!DOCTYPE html>
<html lang="en">
<!--
final index
index.php
Fall 2016
Caleb King
pretty sure none of this works, just started coding...just a heads up
-->
 <head>

   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        // echo '<base href="http://localhost/CIS-425/2167-Fall/Assignments/"';
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Final/cd/grading"';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/">';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="img/pitchfork.png" />

   <!-- link tag for css -->
   <!-- <link type="text/css" rel="stylesheet" href="stylesheets/a1ss.css" /> -->
   <link type="text/css" rel="stylesheet" href="stylesheet/style.css" />

   <!-- javascript tags -->
   <script src="js/meter.js" type="text/javascript"></script>
   <!-- Web Title -->
  <title>Caleb King - Final</title>

 </head>
 <body>
    <div id="wrapper">
      <?php include ('header.php'); ?>
        <div id="progressMeter">
          <p>PLEASE MAKE A SELECTION FROM THE LINKS ABOVE</p>
        </div>
        <p id="jsmsgs"></p>
      <div id="prefooter"></div>
      <?php include('footer.htm'); ?>
    </div>
 </body>
</html>
