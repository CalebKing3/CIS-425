<!DOCTYPE html>
<html lang="en">
<!--
a1
index.php
Fall 2016
Caleb King
-->
 <head>

   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        // echo '<base href="http://localhost/CIS-425/2167-Fall/Assignments/"';
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/"';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/">';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheets/a1ss.css" />

   <!-- javascript tags -->
   <script src="js/meter.js" type="text/javascript"></script>
   <!-- Web Title -->
  <title>Caleb King - A1</title>

 </head>
 <body>
    <div id="wrapper">
      <?php include ('php/header.php'); ?>
        <div id="progressMeter">
          <p>Assignment Progress</p>
          <p><progress id="prog1" max="8"></progress></p>
          <p id="buttons">
              Number of Assignments Completed<br />
              1<input type="radio" name="btns" onchange="meter(1)" />
              2<input type="radio" name="btns" onchange="meter(2)" />
              3<input type="radio" name="btns" onchange="meter(3)" />
              4<input type="radio" name="btns" onchange="meter(4)" />
              5<input type="radio" name="btns" onchange="meter(5)" />
              6<input type="radio" name="btns" onchange="meter(6)" />
              7<input type="radio" name="btns" onchange="meter(7)" />
              8<input type="radio" name="btns" onchange="meter(8)" />
          </p>
        </div>
      <div id="prefooter"></div>
      <?php include('php/footer.htm'); ?>
    </div>
 </body>
</html>
