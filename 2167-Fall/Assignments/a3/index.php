<!DOCTYPE html>
<html lang="en">
<!--
a3
index.php
Fall 2016
Caleb King
-->
 <head>

   <?php
      if(substr($_SERVER['HTTP_HOST'],0,9) == 'localhost')
        echo '<base href="http://localhost:8888/CIS-425/2167-Fall/Assignments/" />';
      else
        echo '<base href="http://cis425.wpcarey.asu.edu/cbking1/"/>';
    ?>

   <!-- Meta tag -->
   <meta charset="utf-8" />

   <!-- favicon link -->
   <link type="image/gif" rel="icon" href="images/home3.png" />

   <!-- link tag for css -->
   <link type="text/css" rel="stylesheet" href="stylesheets/a3ss.css" />

   <!-- javascript tags -->
   <script src="js/meter.js" type="text/javascript"></script>
   <script src="js/a3Focus.js" type="text/javascript"></script>
   <script src="js/a3validate.js" type="text/javascript"></script>
   <!-- Web Title -->
  <title>Caleb King - A3</title>

 </head>
 <body onload="a3Focus()">
    <div id="wrapper">
      <?php include ('../php/header.php'); ?>

      <div id="main">
        <p class="bold">Thank you for your interest in joining my CIS-425 study group</p>
        <p class="note">Please complete the registration form below and click 'SUBMIT' button to get started</p>
      </div>

      <form id="regform" action="php/confirm.php" method="get">
        <p>Registration Form</p>
        <p>
          <!-- Name -->
          <label for="name">Name:</label>
          <input type="text" id="name" name="name" />
          <br />

          <!-- Username -->
          <label for="uname">Username:</label>
          <input type="text" id="uname" name="uname" />
          <br />

          <!-- Password -->
          <label for="pword">Password:</label>
          <input type="password" id="pword" name="pword" />
          <br />

          <!-- Re-enter -->
          <label for="reenter">Re-enter Password:</label>
          <input type="password" id="reenter" name="reenter" />
          <br />

          <!-- Email -->
          <label for="email">Email Address:</label>
          <input type="text" id="email" name="email" />
          <br />

          <!-- Probation -->
          <label for="probation">Academic Probation?</label>
          <input type="checkbox" id="probation" name="probation"/>
          <br />

          <!-- Hot or not -->
          <label for="hony">Hot or not?</label>
          Yes <input type="radio" id="hony" name="hon" value="yes"/>
          No <input type="radio" id="honn" name="hon" value="no" />
          <br />

          <!-- Smart or not -->
          <label for="sony">Smart or not?</label>
          Yes <input type="radio" id="sony" name="son" value="yes"/>
          No <input type="radio" id="sonn" name="son" value="no" />
          <br />

          <!-- Beers. Yay beer! -->
          <label for="beers">Favorite Beer:</label>
          <select id="beers" name="beers">
            <option value="select">Select a beer...</option>
            <option value="heineken">Heineken</option>
            <option value="pbr">Coors Light</option>
            <option value="newcastle">Pabst Blue Ribbon</option>
            <option value="coors">Red's Apple Ale</option>
          </select>
          <br />

          <!-- comments-->
          <label for="comments">Comments:</label><br />
          <textarea id="comments" name="comments" rows="3" cols="48"></textarea>
          <br />
        </p>

        <!-- Submit button -->
        <p class="submit">
            <input type="submit" value="SUBMIT" onclick="return a3validate()"/>
            <span class="reset">
              <input type="reset" value="CLEAR!" onclick="a3Focus()"/>
          </span>
        </p>
      </form>

      <div id="prefooter"></div>
      <?php include('../php/footer.htm'); ?>
    </div>
 </body>
</html>
